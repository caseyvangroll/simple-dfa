const assert = require("assert");
const SimpleDFA = require('../../simple-dfa.js');

const states = ["1","2","3"];
const alphabet = ["a", "b"];
const transitions = {
    1: {'a': '1', 'b': '2'},
    2: {'a': '1', 'b': '3'},
    3: {'a': '2'},
};
const start = "1";
const accept = ["1","2"];

describe("Deep Simulation .deep.simulation", () => {

    it("aaa", () => {
      const dfa = new SimpleDFA(states, alphabet, transitions, start, accept);
      assert.deepEqual(dfa.start('aaa'),  { state: '1', symbol: 'a', accept: true });
      assert.deepEqual(dfa.step(),        { source: '1', symbol: 'a' });
      assert.deepEqual(dfa.step(),        { state: '1', symbol: 'a', accept: true });
      assert.deepEqual(dfa.step(),        { source: '1', symbol: 'a' });
      assert.deepEqual(dfa.step(),        { state: '1', symbol: 'a', accept: true });
      assert.deepEqual(dfa.step(),        { source: '1', symbol: 'a' });
      assert.deepEqual(dfa.step(),        { state: '1', symbol: undefined,  accept: true });
    });
});
