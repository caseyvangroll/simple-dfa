const assert = require("assert");
const SimpleDFA = require('../../simple-dfa.js');

const states = ["1","2","3","4"];
const alphabet = ["a", "b", "c"];
const transitions = {
    1: {'a': '2', 'b': '2', 'c': '3'},
    2: {'b': '3', 'c': '3'},
    3: {'a': '1', 'b': '1', 'c': '1'},
};
const start = "1";
const accept = ["2","3","4"];
let dfa;

describe("Start/Stop Simulation .start.stop.simulation", () => {

  beforeEach(() => {
    dfa = new SimpleDFA(states, alphabet, transitions, start, accept);
  });

  describe ("Legal", () => {
    it("Start simple", () => {
      assert.deepEqual(dfa.start('abc'), { state: '1', symbol: 'a', accept: false });
      assert.deepEqual(dfa.active, { state: '1', symbol: 'a', accept: false });
      assert.equal(dfa.position, 0);
      assert.equal(dfa.input, 'abc');
    });
    it("Start/stop simple", () => {
      assert.deepEqual(dfa.start('abc'), { state: '1', symbol: 'a', accept: false });
      assert.deepEqual(dfa.active, { state: '1', symbol: 'a', accept: false });
      assert.equal(dfa.position, 0);
      assert.equal(dfa.input, 'abc');
      dfa.stop();
      assert.equal(dfa.active, undefined);
      assert.equal(dfa.position, undefined);
      assert.equal(dfa.input, undefined);
    });
  });

  describe ("Illegal", () => {
    it("Bad Input", () => {
      assert.throws(() => dfa.start('abbbababbabcd'));
      assert.throws(() => dfa.start({ bad: 'data' }));
    });
    it("Attempt edit while running", () => {
      dfa.start('a');
      assert.throws(() => dfa.addSymbol('x'));
      assert.throws(() => dfa.removeSymbol('a'));
      assert.throws(() => dfa.addState('5'));
      assert.throws(() => dfa.markStart('2'));
      assert.throws(() => dfa.toggleAccept('3'));
      assert.throws(() => dfa.removeState('3'));
      assert.throws(() => dfa.addTransition('1','4','a'));
      assert.throws(() => dfa.removeTransition('3','1','c'));
      dfa.stop();
      assert.equal(dfa.addSymbol('x'), true);
      assert.equal(dfa.removeSymbol('a'), true);
      assert.equal(dfa.addState('5'), true);
      assert.equal(dfa.markStart('2'), true);
      assert.equal(dfa.toggleAccept('3'), true);
      assert.equal(dfa.removeState('3'), true);
      assert.equal(dfa.addTransition('1','4','a'), true);
      assert.equal(dfa.removeTransition('1','4','a'), true);
    });
  });
});
