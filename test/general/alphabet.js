const assert = require("assert");
const SimpleDFA = require('../../simple-dfa.js');

var states = ['1', '2', '3'];
var alphabet = ["a", "b", "c"];
var transitions = {
    1: {'a': '2', 'b': '2', 'c': '3'},
    2: {'a': '3', 'c': '3'},
    3: {'a': '1', 'b': '1', 'c': '1'},
};
var start = '1';
var accept = ['3'];

describe("Alphabet .alphabet", () => {
  describe("Add symbol to alphabet", () => {
    it("Legal", () => {
      const dfa = new SimpleDFA(states,alphabet,transitions,start,accept);
      assert.equal(dfa.addSymbol("a"), false);
      assert.equal(dfa.alphabet.length, 3);
      assert.equal(dfa.addSymbol("r"), true);
      assert.equal(dfa.alphabet.length, 4);
      assert.equal(dfa.alphabet.includes("r"), true);
    });
    it("Illegal", () => {
      const dfa = new SimpleDFA(states,alphabet,transitions,start,accept);

      assert.throws(() => dfa.addSymbol(true));
      assert.equal(dfa.alphabet.length, 3);

      assert.throws(() => dfa.addSymbol("rra"));
      assert.equal(dfa.alphabet.length, 3);

      assert.throws(() => dfa.addSymbol({ bad: "data" }));
      assert.equal(dfa.alphabet.length, 3);
    });
  });

  describe("Remove symbol from alphabet", () => {
    it("Legal", () => {
      const dfa = new SimpleDFA(states,alphabet,transitions,start,accept);

      assert.equal(dfa.removeSymbol("a"), true);
      assert.equal(dfa.alphabet.length, 2);
      assert.equal(dfa.alphabet.includes("a"), false);
      assert.equal(dfa.transitions['1']['a'], undefined);
      assert.equal(dfa.transitions['2']['a'], undefined);
      assert.equal(dfa.transitions['3']['a'], undefined);

      assert.equal(dfa.removeSymbol("c"), true);
      assert.equal(dfa.alphabet.length, 1);
      assert.equal(dfa.alphabet.includes("c"), false);
      assert.equal(dfa.transitions['1']['c'], undefined);
      assert.equal(dfa.transitions['2'], undefined);
      assert.equal(dfa.transitions['3']['c'], undefined);

      assert.equal(dfa.removeSymbol("b"), true);
      assert.equal(dfa.alphabet.length, 0);
      assert.equal(dfa.alphabet.includes("b"), false);
      assert.equal(dfa.transitions['1'], undefined);
      assert.equal(dfa.transitions['2'], undefined);
      assert.equal(dfa.transitions['3'], undefined);

      assert.equal(dfa.removeSymbol("b"), false);
    });
    it("Illegal", () => {
      const dfa = new SimpleDFA(states,alphabet,transitions,start,accept);
      assert.throws(() => dfa.removeSymbol("ab"));
      assert.throws(() => dfa.removeSymbol(true));
      assert.throws(() => dfa.removeSymbol("rra"));
      assert.throws(() => dfa.removeSymbol({ bad: "data" }));
      assert.equal(dfa.alphabet.length, 3);
    });
  });
});
