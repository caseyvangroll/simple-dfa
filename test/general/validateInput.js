const assert = require("assert");
const SimpleDFA = require('../../simple-dfa.js');

describe("Validate Input .validate", () => {
  const dfa = new SimpleDFA([], ["a", "b", "c"], []);

  describe ("Legal", () => {
    it("Valid", () => {
      assert.equal(dfa.validateInput(""), true);
      assert.equal(dfa.validateInput("a"), true);
      assert.equal(dfa.validateInput("b"), true);
      assert.equal(dfa.validateInput("c"), true);
      assert.equal(dfa.validateInput("acbc"), true);
      assert.equal(dfa.validateInput("bcabcabcbca"), true);
      assert.equal(dfa.validateInput("acbabcbbbbbbbbbbbbbbbbbbbbbbbbbbb"), true);
    });

    it("Invalid", () => {
      assert.equal(dfa.validateInput("d"), false);
      assert.equal(dfa.validateInput("abcdc"), false);
      assert.equal(dfa.validateInput(" "), false);
      assert.equal(dfa.validateInput("aaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbf"), false);
    });
  });
  describe ("Illegal", () => {
    it("Non-String", () => {
      assert.throws(() => dfa.validateInput({bad: "data"}));
    });
  });
});
