const assert = require('assert');
const SimpleDFA = require('../../simple-dfa.js');

const good = {
  states: {
    raw: ['1', '2', '3'],
    parsed: {
      '1': { start: true, accept: false},
      '2': { start: false, accept: false},
      '3': { start: false, accept: false},
    }
  },
  alphabet: ["a", "b", "c"],
  transitions: {
      1: {'a': '2', 'b': '2', 'c': '3'},
      2: {'b': '3', 'c': '3'},
      3: {'a': '1', 'b': '1', 'c': '1'}
  },
  start: '1',
  accept: ['3'],
}

const bad = {
    states: [
      {bad: "data"},
      [{bad: "data"}],
      ['1', '2', {bad: "data"}],
    ],
    alphabet: [
      {bad: "data"},
      ["c", "b", true]
    ],
    transitions: [
      {bad: "data"},
      [{bad: "data"}],
      {
        1: {'a': '2', 'b': '2', 'c': '3'},
        1: {'b': '3', 'c': '3'},
      },
      {
        1: {'a': '2', 'b': '2', 'c': '3'},
        2: {'b': '3', 'c': '3'},
        3: {'a': '1', 'b': '1', 'j': '1'},
      },
      {
        1: {'a': '2', 'b': '2', 'c': '30'},
        2: {'b': '3', 'c': '3'},
        3: {'a': '1', 'b': '1', 'j': '1'},
      },
    ],
};

describe('Construction .construction', () => {

    it('No parameters', () => {
      const dfa = new SimpleDFA();
      assert.deepEqual(dfa.states, {});
      assert.deepEqual(dfa.alphabet, []);
      assert.deepEqual(dfa.transitions, {});
    })

    describe('Parameters', () => {
      it('Good 1', () => {
        const dfa = new SimpleDFA(good.states.raw, good.alphabet, good.transitions, good.start);
        assert.deepEqual(dfa.states, good.states.parsed);
        assert.deepEqual(dfa.alphabet, good.alphabet);
        assert.deepEqual(dfa.transitions, good.transitions);
      })

      it('Good 2', () => {
        const dfa = new SimpleDFA(["1"], good.alphabet, undefined, "1");
      })

      describe("Bad", () => {

        bad.states.forEach(badStates => {
          it('States', () => {
            assert.throws(() => new SimpleDFA(badStates, undefined, undefined, good.start));
          })
        })

        bad.alphabet.forEach(badAlphabet => {
          it('Alphabet', () => {
            assert.throws(() => new SimpleDFA(undefined, badAlphabet));
          });
        })

        bad.transitions.forEach(badTransitions => {
          it('Transitions', () => {
            assert.throws(() => new SimpleDFA(good.states, good.alphabet, badTransitions, good.start));
          });
        })
      })
    })
})