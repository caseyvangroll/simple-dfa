const assert = require("assert");
const SimpleDFA = require('../../simple-dfa.js');

const states = ["1","2","3","4"];
const alphabet = ["a", "b", "c"];
const transitions = {
    1: {'a': '2', 'b': '2', 'c': '3'},
    2: {'b': '3', 'c': '3'},
    3: {'a': '1', 'b': '1', 'c': '1'},
};
const start = "1";
const accept = ["2","3","4"];
let dfa;

describe("Remove Transition .remove.transition", () => {

  beforeEach(() => {
    dfa = new SimpleDFA(states, alphabet, transitions, start, accept);
  });

  describe ("Legal", () => {
    it("Remove nonexistent", () => {
      assert.equal(dfa.removeTransition('1','5','a'), false);
      assert.equal(dfa.removeTransition('a','2','a'), false);
      assert.equal(dfa.removeTransition('1','t','b'), false);
      assert.equal(dfa.removeTransition('1','2','d'), false);
      assert.equal(dfa.removeTransition('1','2','c'), false);
      assert.equal(dfa.removeTransition('2','3','a'), false);
      assert.equal(dfa.removeTransition('2','1','b'), false);
      assert.equal(dfa.removeTransition('3','1','d'), false);
      assert.equal(dfa.removeTransition('3','2','a'), false);
    });

    it("Remove same twice", () => {
      assert.equal(dfa.removeTransition('1','2','a'), true);
      assert.ok(!dfa.transitions['1']['a']);
      assert.equal(dfa.removeTransition('1','2','a'), false);
    });

    it("Remove all from single", () => {
      assert.equal(dfa.removeTransition('1','2','a'), true);
      assert.equal(dfa.removeTransition('1','2','b'), true);
      assert.ok(!dfa.transitions['1']['a']);
      assert.ok(!dfa.transitions['1']['b']);
      assert.equal(dfa.removeTransition('1','3','c'), true);
    });

    it("Remove various", () => {
      assert.equal(dfa.removeTransition('1','3','c'), true);
      assert.equal(dfa.removeTransition('1','2','b'), true);
      assert.equal(dfa.removeTransition('3','1','b'), true);
      assert.equal(dfa.removeTransition('3','1','a'), true);

      assert.ok(!dfa.transitions['1']['c']);
      assert.ok(!dfa.transitions['1']['b']);
      assert.ok(!dfa.transitions['3']['b']);
      assert.ok(!dfa.transitions['3']['a']);
    });
  });



  describe ("Illegal", () => {
    it("Bad Parameters", () => {
      assert.throws(() => dfa.addTransition());
      assert.throws(() => dfa.addTransition({bad: "data"}));
      assert.throws(() => dfa.addTransition("h",{bad: "data"}, "e"));
      assert.throws(() => dfa.addTransition("h","e",{bad: "data"}));
      assert.throws(() => dfa.addTransition(["h"],["e"],["o"]));
      assert.throws(() => dfa.addTransition(["h","e","o"]));
    });
  });
});
