const assert = require("assert");
const SimpleDFA = require('../../simple-dfa.js');

const states = ["1","2","3","4"];
const alphabet = ["a", "b", "c"];
const transitions = {
    1: {'a': '2', 'b': '2', 'c': '3'},
    2: {'b': '3', 'c': '3'},
    3: {'a': '1', 'b': '1', 'c': '1'},
};
const start = "1";
const accept = ["2","3","4"];

describe("Add Transition .add.transition", () => {
  describe ("Legal", () => {
    it("Add to empty, vertices/symbol exist", () => {
      const dfa = new SimpleDFA(['1','2'], ['a'], undefined, '1');
      assert.equal(dfa.addTransition('1','2','a'), true);
      assert.ok(dfa.transitions['1']);
      assert.equal(dfa.transitions['1']['a'], '2');
      assert.ok(!dfa.transitions['2']);
    });
    it("Add to empty, vertices exist, symbol doesn't", () => {
        const dfa = new SimpleDFA(['1','2'], undefined, undefined, '1');
        assert.equal(dfa.addTransition('1','2','a'), true);
        assert.equal(dfa.alphabet.includes('a'), true);
        assert.ok(dfa.transitions['1']);
        assert.equal(dfa.transitions['1']['a'], '2');
        assert.ok(!dfa.transitions['2']);
    });
    it("Add to empty, nothing exists", () => {
        const dfa = new SimpleDFA();
        assert.equal(dfa.addTransition('1','2','a'), true);
        assert.ok(dfa.states['1']);
        assert.deepEqual(dfa.states['1'], { start: true, accept: false });
        assert.ok(dfa.states['2']);
        assert.deepEqual(dfa.states['2'], { start: false, accept: false });
        assert.equal(dfa.alphabet.includes('a'), true);
        assert.ok(dfa.transitions['1']);
        assert.equal(dfa.transitions['1']['a'], '2');
        assert.ok(!dfa.transitions['2']);
    });
    it("Add to non-empty", () => {
      const dfa = new SimpleDFA(states, alphabet, transitions, start, accept);
      assert.equal(dfa.addTransition('1','5','d'), true);
      assert.equal(dfa.transitions['1']['d'], '5');
      assert.deepEqual(dfa.states['5'], { start: false, accept: false });
      assert.ok(!dfa.transitions['5']);

      assert.equal(dfa.addTransition('5','1','a'), true);
      assert.equal(dfa.addTransition('5','1','a'), false);
  });
    it("Transition already exists", () => {
        const dfa = new SimpleDFA(states, alphabet, transitions, start, accept);
        Object.keys(transitions).forEach(sourceStateID => {
          Object.keys(transitions[sourceStateID]).forEach(symbol => {
            const destination = transitions[sourceStateID][symbol];
            assert.equal(dfa.addTransition(sourceStateID, destination, symbol), false);
          });
        });
    });
  });
  describe ("Illegal", () => {
    it("Bad Parameters", () => {
      const dfa = new SimpleDFA(states, alphabet, transitions, start, accept);
      assert.throws(() => dfa.addTransition());
      assert.throws(() => dfa.addTransition({bad: "data"}));
      assert.throws(() => dfa.addTransition("h",{bad: "data"}, "e"));
      assert.throws(() => dfa.addTransition("h","e",{bad: "data"}));
      assert.throws(() => dfa.addTransition(["h"],["e"],["o"]));
      assert.throws(() => dfa.addTransition(["h","e","o"]));
    });
  });
});
