const assert = require("assert");
const SimpleDFA = require('../../simple-dfa.js');

const states = ["1","2","3","4","5"];
const alphabet = ["a", "b", "c"];
const transitions = {
    1: {'a': '2', 'b': '2', 'c': '3'},
    2: {'b': '3', 'c': '3'},
    3: {'a': '1', 'b': '1', 'c': '1'},
    4: {'a': '1', 'b': '1', 'c': '1'},
};
const start = "1";
const accept = ["2","3","4","5"];
let dfa;

describe("Remove State .remove.state", () => {

  beforeEach(() => {
    dfa = new SimpleDFA(states, alphabet, transitions, start, accept);
  });

  describe ("Legal", () => {
    it("Remove Island/Nonexistent", () => {
      assert.equal(dfa.removeState("5"), true);
      assert.equal(dfa.states['5'], undefined);
      assert.equal(dfa.removeState("6"), false);
    });

    describe("Remove Connected", () => {

      it("1 Transition", () => {
        assert.equal(dfa.removeState("4"), true);
        assert.equal(dfa.states['4'], undefined);
        assert.equal(dfa.transitions['4'], undefined);
      });

      it("2 Transitions", () => {
        assert.equal(dfa.removeState("2"), true);
        assert.equal(dfa.states['2'], undefined);
        assert.equal(dfa.transitions['2'], undefined);
        assert.equal(dfa.transitions['1']['a'], undefined);
        assert.equal(dfa.transitions['1']['b'], undefined);
      });

      it("3 Transitions", () => {
        assert.equal(dfa.removeState("3"), true);
        assert.equal(dfa.states['3'], undefined);
        assert.equal(dfa.transitions['3'], undefined);
        assert.equal(dfa.transitions['1']['c'], undefined);
        assert.equal(dfa.transitions['2']['b'], undefined);
        assert.equal(dfa.transitions['2']['c'], undefined);
      });

      it("4 Transitions", () => {
        assert.throws(() => dfa.removeState("1")); // Can't delete start state
        dfa.markStart('2');
        assert.equal(dfa.removeState("1"), true);

        assert.equal(dfa.states['1'], undefined);
        assert.equal(dfa.transitions['1'], undefined);
        assert.equal(dfa.transitions['3']['a'], undefined);
        assert.equal(dfa.transitions['3']['b'], undefined);
        assert.equal(dfa.transitions['3']['c'], undefined);
        assert.equal(dfa.transitions['4']['a'], undefined);
        assert.equal(dfa.transitions['4']['b'], undefined);
        assert.equal(dfa.transitions['4']['c'], undefined);
      });
    });

  });

  describe ("Illegal", () => {
    it("Non-State Parameter", () => {
      assert.throws(() => dfa.removeState({bad: "data"}));
    });
  });
});
