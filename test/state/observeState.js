const assert = require("assert");
const SimpleDFA = require('../../simple-dfa.js');

const states = ["1","2","3","4"];
const alphabet = ["a", "b", "c"];
const transitions = {
    1: {'a': '2', 'b': '2', 'c': '3'},
    2: {'b': '3', 'c': '3'},
    3: {'a': '1', 'b': '1', 'c': '1'},
};
const start = "1";
const accept = ["2","3","4"];

describe("Observe State .observe.state", () => {
  const dfa = new SimpleDFA(states, alphabet, transitions, start, accept);

  describe ("Legal", () => {
    it("Observe Connected", () => {
      assert.equal(dfa.isConnected("1"), true);
      assert.equal(dfa.isConnected("2"), true);
      assert.equal(dfa.isConnected("3"), true);
      assert.equal(dfa.isConnected("4"), false);
      assert.equal(dfa.isConnected("-1"), false);
    });
  });
  describe ("Illegal", () => {
    it("Observe Connected", () => {
      assert.throws(() => dfa.isConnected({bad: "data"}));
    });
  });
});
