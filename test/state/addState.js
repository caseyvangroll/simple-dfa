const assert = require("assert");
const SimpleDFA = require('../../simple-dfa.js');

const states = ["1","2","3"];
const start = "1";
const accept = ["2","3"];

describe("Add State .add.state", () => {
  it("To empty", () => {
    const dfa = new SimpleDFA();
    assert.equal(dfa.addState("1"), true);
    assert.deepEqual(dfa.states["1"], { start: true, accept: false });
  });

  it("To existing", () => {
    const dfa = new SimpleDFA(states, undefined, undefined, start, accept);
    assert.equal(dfa.addState("4"), true);
    assert.deepEqual(dfa.states["4"], { start: false, accept: false });
    assert.equal(dfa.addState("4"), false);
  });

  it("Illegal", () => {
    const dfa = new SimpleDFA();
    assert.throws(() => dfa.addState(true));
    assert.throws(() => dfa.addState({ bad: "data" }));
  });
});
