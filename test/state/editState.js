const assert = require("assert");
const SimpleDFA = require('../../simple-dfa.js');

const states = ["1","2","3"];
const start = "1";
const accept = ["2","3"];

describe("Edit State .edit.state", () => {
  it("Mark Start", () => {
    const dfa = new SimpleDFA(states, undefined, undefined, start, accept);
    assert.equal(dfa.states["1"].start, true);
    assert.equal(dfa.markStart("1"), false);

    assert.equal(dfa.markStart("2"), true);
    assert.equal(dfa.states["1"].start, false);
    assert.equal(dfa.states["2"].start, true);

    assert.throws(() => dfa.markStart("5"));
    assert.equal(dfa.states["2"].start, true);
  });
  it("Toggle Accept", () => {
    const dfa = new SimpleDFA(states, undefined, undefined, start);
    dfa.toggleAccept("3");
    assert.equal(dfa.states["1"].accept, false);
    assert.equal(dfa.states["2"].accept, false);
    assert.equal(dfa.states["3"].accept, true);

    assert.throws(() => dfa.toggleAccept("12"));
    assert.equal(dfa.states["1"].accept, false);
    assert.equal(dfa.states["2"].accept, false);
    assert.equal(dfa.states["3"].accept, true);

    assert.equal(dfa.toggleAccept("1"), true);
    assert.equal(dfa.toggleAccept("2"), true);
    assert.equal(dfa.toggleAccept("3"), true);
    assert.equal(dfa.states["1"].accept, true);
    assert.equal(dfa.states["2"].accept, true);
    assert.equal(dfa.states["3"].accept, false);
  });
});
